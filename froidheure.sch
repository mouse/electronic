EESchema Schematic File Version 4
LIBS:froidheure-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 1950 3450 2    50   ~ 0
HV_HL0
Text Label 1950 3550 2    50   ~ 0
HV_HL1
Text Label 1950 3650 2    50   ~ 0
HV_HL2
Text Label 1950 3750 2    50   ~ 0
HV_HL3
Text Label 1950 3850 2    50   ~ 0
HV_HL4
Text Label 1950 3950 2    50   ~ 0
HV_HL5
Text Label 1950 4050 2    50   ~ 0
HV_HL6
Text Label 1950 4150 2    50   ~ 0
HV_HL7
Text Label 1950 4250 2    50   ~ 0
HV_HL8
Text Label 1950 4350 2    50   ~ 0
HV_HL9
Text Label 10600 3450 2    50   ~ 0
HV_SL0
Text Label 10600 3550 2    50   ~ 0
HV_SL1
Text Label 10600 3650 2    50   ~ 0
HV_SL2
Text Label 10600 3750 2    50   ~ 0
HV_SL3
Text Label 10600 3850 2    50   ~ 0
HV_SL4
Text Label 10600 3950 2    50   ~ 0
HV_SL5
Text Label 10600 4050 2    50   ~ 0
HV_SL6
Text Label 10600 4150 2    50   ~ 0
HV_SL7
Text Label 10600 4250 2    50   ~ 0
HV_SL8
Text Label 10600 4350 2    50   ~ 0
HV_SL9
Text Label 11850 3450 2    50   ~ 0
HV_SR0
Text Label 11850 3550 2    50   ~ 0
HV_SR1
Text Label 11850 3650 2    50   ~ 0
HV_SR2
Text Label 11850 3750 2    50   ~ 0
HV_SR3
Text Label 11850 3850 2    50   ~ 0
HV_SR4
Text Label 11850 3950 2    50   ~ 0
HV_SR5
Text Label 11850 4050 2    50   ~ 0
HV_SR6
Text Label 11850 4150 2    50   ~ 0
HV_SR7
Text Label 11850 4250 2    50   ~ 0
HV_SR8
Text Label 11850 4350 2    50   ~ 0
HV_SR9
Text Label 6250 3450 2    50   ~ 0
HV_ML0
Text Label 6250 3550 2    50   ~ 0
HV_ML1
Text Label 6250 3650 2    50   ~ 0
HV_ML2
Text Label 6250 3750 2    50   ~ 0
HV_ML3
Text Label 6250 3850 2    50   ~ 0
HV_ML4
Text Label 6250 3950 2    50   ~ 0
HV_ML5
Text Label 6250 4050 2    50   ~ 0
HV_ML6
Text Label 6250 4150 2    50   ~ 0
HV_ML7
Text Label 6250 4250 2    50   ~ 0
HV_ML8
Text Label 6250 4350 2    50   ~ 0
HV_ML9
Text Label 7500 3450 2    50   ~ 0
HV_MR0
Text Label 7500 3550 2    50   ~ 0
HV_MR1
Text Label 7500 3650 2    50   ~ 0
HV_MR2
Text Label 7500 3750 2    50   ~ 0
HV_MR3
Text Label 7500 3850 2    50   ~ 0
HV_MR4
Text Label 7500 3950 2    50   ~ 0
HV_MR5
Text Label 7500 4050 2    50   ~ 0
HV_MR6
Text Label 7500 4150 2    50   ~ 0
HV_MR7
Text Label 7500 4250 2    50   ~ 0
HV_MR8
Text Label 7500 4350 2    50   ~ 0
HV_MR9
Text Label 3200 3850 2    50   ~ 0
HV_HR4
Text Label 3200 3950 2    50   ~ 0
HV_HR5
Text Label 3200 4050 2    50   ~ 0
HV_HR6
Text Label 3200 4150 2    50   ~ 0
HV_HR7
Text Label 3200 4250 2    50   ~ 0
HV_HR8
Text Label 3200 4350 2    50   ~ 0
HV_HR9
Text Label 5850 7800 2    50   ~ 0
HV_HL0
Text Label 5850 7700 2    50   ~ 0
HV_HL1
Text Label 5850 7600 2    50   ~ 0
HV_HL2
Text Label 5850 7500 2    50   ~ 0
HV_HL3
Text Label 5850 7400 2    50   ~ 0
HV_HL4
Text Label 5850 7300 2    50   ~ 0
HV_HL5
Text Label 5850 7200 2    50   ~ 0
HV_HL6
Text Label 5850 7100 2    50   ~ 0
HV_HL7
Text Label 5850 7000 2    50   ~ 0
HV_HL8
Text Label 5850 6900 2    50   ~ 0
HV_HL9
Text Label 5850 8800 2    50   ~ 0
HV_HR0
Text Label 5850 8700 2    50   ~ 0
HV_HR1
Text Label 5850 8600 2    50   ~ 0
HV_HR2
Text Label 5850 8500 2    50   ~ 0
HV_HR3
Text Label 5850 8400 2    50   ~ 0
HV_HR4
Text Label 5850 8300 2    50   ~ 0
HV_HR5
Text Label 5850 8200 2    50   ~ 0
HV_HR6
Text Label 5850 8100 2    50   ~ 0
HV_HR7
Text Label 5850 8000 2    50   ~ 0
HV_HR8
Text Label 5850 7900 2    50   ~ 0
HV_HR9
Text Label 8850 7900 0    50   ~ 0
HV_ML0
Text Label 8850 8000 0    50   ~ 0
HV_ML1
Text Label 8850 8100 0    50   ~ 0
HV_ML2
Text Label 8850 8200 0    50   ~ 0
HV_ML3
Text Label 8850 8300 0    50   ~ 0
HV_ML4
Text Label 8850 8400 0    50   ~ 0
HV_ML5
Text Label 8850 8500 0    50   ~ 0
HV_ML6
Text Label 8850 8600 0    50   ~ 0
HV_ML7
Text Label 8850 8700 0    50   ~ 0
HV_ML8
Text Label 8850 8800 0    50   ~ 0
HV_ML9
Text Label 8850 6900 0    50   ~ 0
HV_MR0
Text Label 8850 7000 0    50   ~ 0
HV_MR1
Text Label 8850 7100 0    50   ~ 0
HV_MR2
Text Label 8850 7200 0    50   ~ 0
HV_MR3
Text Label 8850 7300 0    50   ~ 0
HV_MR4
Text Label 8850 7400 0    50   ~ 0
HV_MR5
Text Label 8850 7500 0    50   ~ 0
HV_MR6
Text Label 8850 7600 0    50   ~ 0
HV_MR7
Text Label 8850 7700 0    50   ~ 0
HV_MR8
Text Label 8850 7800 0    50   ~ 0
HV_MR9
Text Label 7400 6350 1    50   ~ 0
HV_SL0
Text Label 7500 6350 1    50   ~ 0
HV_SL1
Text Label 7600 6350 1    50   ~ 0
HV_SL2
Text Label 7700 6350 1    50   ~ 0
HV_SL3
Text Label 7800 6350 1    50   ~ 0
HV_SL4
Text Label 7900 6350 1    50   ~ 0
HV_SL5
Text Label 8000 6350 1    50   ~ 0
HV_SL6
Text Label 8100 6350 1    50   ~ 0
HV_SL7
Text Label 8200 6350 1    50   ~ 0
HV_SL8
Text Label 8300 6350 1    50   ~ 0
HV_SL9
Text Label 6400 6350 1    50   ~ 0
HV_SR0
Text Label 6500 6350 1    50   ~ 0
HV_SR1
Text Label 6600 6350 1    50   ~ 0
HV_SR2
Text Label 6700 6350 1    50   ~ 0
HV_SR3
Text Label 6800 6350 1    50   ~ 0
HV_SR4
Text Label 6900 6350 1    50   ~ 0
HV_SR5
Text Label 7000 6350 1    50   ~ 0
HV_SR6
Text Label 7100 6350 1    50   ~ 0
HV_SR7
Text Label 7200 6350 1    50   ~ 0
HV_SR8
Text Label 7300 6350 1    50   ~ 0
HV_SR9
$Comp
L LED:APA102 D1
U 1 1 5D2A8530
P 2400 5450
F 0 "D1" H 2596 5813 50  0000 C CNN
F 1 "APA102" H 2596 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 2450 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 2500 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 2400 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 2400 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 2400 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 2400 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 2400 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 2400 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 2400 5450 50  0001 L CNN "DeviceFournisseur"
	1    2400 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4550 4500 4650
$Comp
L power:GND #PWR01
U 1 1 5D2E14B6
P 1750 4650
F 0 "#PWR01" H 1750 4400 50  0001 C CNN
F 1 "GND" H 1755 4477 50  0001 C CNN
F 2 "" H 1750 4650 50  0001 C CNN
F 3 "" H 1750 4650 50  0001 C CNN
	1    1750 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5D2CAB09
P 4600 4550
F 0 "R3" V 4600 4380 50  0000 C CNN
F 1 "R_Small" V 4495 4550 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4600 4550 50  0001 C CNN
F 3 "~" H 4600 4550 50  0001 C CNN
	1    4600 4550
	0    1    1    0   
$EndComp
Text Label 4700 4450 2    50   ~ 0
HV_SLB
Connection ~ 4500 4650
$Comp
L power:GND #PWR07
U 1 1 5D2D8F98
P 4500 4650
F 0 "#PWR07" H 4500 4400 50  0001 C CNN
F 1 "GND" H 4505 4477 50  0001 C CNN
F 2 "" H 4500 4650 50  0001 C CNN
F 3 "" H 4500 4650 50  0001 C CNN
	1    4500 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R4
U 1 1 5D2CBF0D
P 4600 4650
F 0 "R4" V 4580 4482 50  0000 C CNN
F 1 "R_Small" V 4692 4689 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4600 4650 50  0001 C CNN
F 3 "~" H 4600 4650 50  0001 C CNN
	1    4600 4650
	0    1    1    0   
$EndComp
Wire Wire Line
	8850 4550 8850 4650
Connection ~ 8850 4650
$Comp
L power:GND #PWR018
U 1 1 5D31183A
P 8850 4650
F 0 "#PWR018" H 8850 4400 50  0001 C CNN
F 1 "GND" H 8855 4477 50  0001 C CNN
F 2 "" H 8850 4650 50  0001 C CNN
F 3 "" H 8850 4650 50  0001 C CNN
	1    8850 4650
	1    0    0    -1  
$EndComp
Text Label 13400 8150 2    50   ~ 0
IC2_DTR
Text Label 12100 8050 0    50   ~ 0
IC2_DTR
NoConn ~ 12600 5350
NoConn ~ 12600 5450
Text Label 2100 5450 2    50   ~ 0
APA_CLK
Text Label 2100 5350 2    50   ~ 0
APA_SDI
$Comp
L power:+5V #PWR02
U 1 1 5D43E330
P 2400 5150
F 0 "#PWR02" H 2400 5000 50  0001 C CNN
F 1 "+5V" H 2415 5323 50  0000 C CNN
F 2 "" H 2400 5150 50  0001 C CNN
F 3 "" H 2400 5150 50  0001 C CNN
	1    2400 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D2
U 1 1 5D4444F8
P 3650 5450
F 0 "D2" H 3846 5813 50  0000 C CNN
F 1 "APA102" H 3846 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 3700 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 3750 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 3650 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 3650 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 3650 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 3650 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 3650 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 3650 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 3650 5450 50  0001 L CNN "DeviceFournisseur"
	1    3650 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5D4444FE
P 3650 5150
F 0 "#PWR05" H 3650 5000 50  0001 C CNN
F 1 "+5V" H 3665 5323 50  0000 C CNN
F 2 "" H 3650 5150 50  0001 C CNN
F 3 "" H 3650 5150 50  0001 C CNN
	1    3650 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D3
U 1 1 5D445557
P 4600 5450
F 0 "D3" H 4796 5813 50  0000 C CNN
F 1 "APA102" H 4796 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 4650 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 4700 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 4600 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 4600 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 4600 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 4600 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 4600 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 4600 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 4600 5450 50  0001 L CNN "DeviceFournisseur"
	1    4600 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR08
U 1 1 5D44555D
P 4600 5150
F 0 "#PWR08" H 4600 5000 50  0001 C CNN
F 1 "+5V" H 4615 5323 50  0000 C CNN
F 2 "" H 4600 5150 50  0001 C CNN
F 3 "" H 4600 5150 50  0001 C CNN
	1    4600 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D5
U 1 1 5D446CFD
P 6700 5450
F 0 "D5" H 6896 5813 50  0000 C CNN
F 1 "APA102" H 6896 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 6750 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 6800 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 6700 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 6700 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 6700 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 6700 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 6700 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 6700 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 6700 5450 50  0001 L CNN "DeviceFournisseur"
	1    6700 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR013
U 1 1 5D446D03
P 6700 5150
F 0 "#PWR013" H 6700 5000 50  0001 C CNN
F 1 "+5V" H 6715 5323 50  0000 C CNN
F 2 "" H 6700 5150 50  0001 C CNN
F 3 "" H 6700 5150 50  0001 C CNN
	1    6700 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D6
U 1 1 5D447FC9
P 7950 5450
F 0 "D6" H 8146 5813 50  0000 C CNN
F 1 "APA102" H 8146 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 8000 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 8050 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 7950 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 7950 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 7950 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 7950 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 7950 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 7950 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 7950 5450 50  0001 L CNN "DeviceFournisseur"
	1    7950 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR016
U 1 1 5D447FCF
P 7950 5150
F 0 "#PWR016" H 7950 5000 50  0001 C CNN
F 1 "+5V" H 7965 5323 50  0000 C CNN
F 2 "" H 7950 5150 50  0001 C CNN
F 3 "" H 7950 5150 50  0001 C CNN
	1    7950 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D8
U 1 1 5D449E71
P 9950 5450
F 0 "D8" H 10146 5813 50  0000 C CNN
F 1 "APA102" H 10146 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 10000 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 10050 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 9950 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 9950 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 9950 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 9950 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 9950 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 9950 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 9950 5450 50  0001 L CNN "DeviceFournisseur"
	1    9950 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR021
U 1 1 5D449E77
P 9950 5150
F 0 "#PWR021" H 9950 5000 50  0001 C CNN
F 1 "+5V" H 9965 5323 50  0000 C CNN
F 2 "" H 9950 5150 50  0001 C CNN
F 3 "" H 9950 5150 50  0001 C CNN
	1    9950 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D9
U 1 1 5D44B13D
P 11050 5450
F 0 "D9" H 11246 5813 50  0000 C CNN
F 1 "APA102" H 11246 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 11100 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 11150 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 11050 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 11050 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 11050 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 11050 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 11050 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 11050 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 11050 5450 50  0001 L CNN "DeviceFournisseur"
	1    11050 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR028
U 1 1 5D44B143
P 11050 5150
F 0 "#PWR028" H 11050 5000 50  0001 C CNN
F 1 "+5V" H 11065 5323 50  0000 C CNN
F 2 "" H 11050 5150 50  0001 C CNN
F 3 "" H 11050 5150 50  0001 C CNN
	1    11050 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D10
U 1 1 5D44D127
P 12300 5450
F 0 "D10" H 12496 5813 50  0000 C CNN
F 1 "APA102" H 12496 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 12350 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 12400 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 12300 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 12300 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 12300 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 12300 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 12300 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 12300 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 12300 5450 50  0001 L CNN "DeviceFournisseur"
	1    12300 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR034
U 1 1 5D44D12D
P 12300 5150
F 0 "#PWR034" H 12300 5000 50  0001 C CNN
F 1 "+5V" H 12315 5323 50  0000 C CNN
F 2 "" H 12300 5150 50  0001 C CNN
F 3 "" H 12300 5150 50  0001 C CNN
	1    12300 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D4
U 1 1 5D44E255
P 5600 5450
F 0 "D4" H 5796 5813 50  0000 C CNN
F 1 "APA102" H 5796 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 5650 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 5700 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 5600 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 5600 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 5600 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 5600 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 5600 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 5600 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 5600 5450 50  0001 L CNN "DeviceFournisseur"
	1    5600 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR010
U 1 1 5D44E25B
P 5600 5150
F 0 "#PWR010" H 5600 5000 50  0001 C CNN
F 1 "+5V" H 5615 5323 50  0000 C CNN
F 2 "" H 5600 5150 50  0001 C CNN
F 3 "" H 5600 5150 50  0001 C CNN
	1    5600 5150
	1    0    0    -1  
$EndComp
$Comp
L LED:APA102 D7
U 1 1 5D4505C0
P 8950 5450
F 0 "D7" H 9146 5813 50  0000 C CNN
F 1 "APA102" H 9146 5722 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_PLCC-6" H 9000 5150 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 9050 5075 50  0001 L TNN
F 4 "APA Electronic Co., Ltd" H 8950 5450 50  0001 L CNN "Fabricant"
F 5 "APA-102C-256" H 8950 5450 50  0001 L CNN "RefFabricant"
F 6 "AlyExpress" H 8950 5450 50  0001 L CNN "Fournisseur"
F 7 "32895268773" H 8950 5450 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.aliexpress.com/item/32895268773.html" H 8950 5450 50  0001 L CNN "LinkFournisseur"
F 9 "0,234" H 8950 5450 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 8950 5450 50  0001 L CNN "DeviceFournisseur"
	1    8950 5450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR019
U 1 1 5D4505C6
P 8950 5150
F 0 "#PWR019" H 8950 5000 50  0001 C CNN
F 1 "+5V" H 8965 5323 50  0000 C CNN
F 2 "" H 8950 5150 50  0001 C CNN
F 3 "" H 8950 5150 50  0001 C CNN
	1    8950 5150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR038
U 1 1 5D461CA5
P 14000 6350
F 0 "#PWR038" H 14000 6200 50  0001 C CNN
F 1 "+5V" H 14015 6523 50  0000 C CNN
F 2 "" H 14000 6350 50  0001 C CNN
F 3 "" H 14000 6350 50  0001 C CNN
	1    14000 6350
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5D4624C9
P 14000 9350
F 0 "#PWR039" H 14000 9100 50  0001 C CNN
F 1 "GND" H 14005 9177 50  0001 C CNN
F 2 "" H 14000 9350 50  0001 C CNN
F 3 "" H 14000 9350 50  0001 C CNN
	1    14000 9350
	-1   0    0    -1  
$EndComp
$Comp
L Interface_USB:CH340G U2
U 1 1 5D245E4A
P 11700 7750
F 0 "U2" H 11818 7179 50  0000 C CNN
F 1 "CH340G" H 11917 7089 50  0000 C CNN
F 2 "Package_SO:SOIC-16_3.9x9.9mm_P1.27mm" H 11750 7200 50  0001 L CNN
F 3 "http://www.datasheet5.com/pdf-local-2195953" H 11350 8550 50  0001 L CNN
F 4 "DreamCity Innovations" H 11700 7750 50  0001 L CNN "Fabricant"
F 5 "CH340G" H 11700 7750 50  0001 L CNN "RefFabricant"
F 6 "LCSC" H 11700 7750 50  0001 L CNN "Fournisseur"
F 7 "USB_CH340G_C14267" H 11700 7750 50  0001 L CNN "RefFournisseur"
F 8 "https://lcsc.com/product-detail/USB_CH340G_C14267.html" H 11700 7750 50  0001 L CNN "LinkFournisseur"
F 9 "https://datasheet.lcsc.com/szlcsc/Jiangsu-Qin-Heng-CH340G_C14267.pdf" H 11700 7750 50  0001 L CNN "DatasheetFournisseur"
F 10 "0.4061" H 11700 7750 50  0001 L CNN "CostFournisseur"
F 11 "USD" H 11700 7750 50  0001 L CNN "DeviceFournisseur"
	1    11700 7750
	1    0    0    -1  
$EndComp
NoConn ~ 11300 7450
Text Label 12100 7450 0    50   ~ 0
IC2_RX
Text Label 12100 7350 0    50   ~ 0
IC2_TX
Text Label 12100 7650 0    50   ~ 0
IC2_XLK
Text Label 12100 7750 0    50   ~ 0
IC2_DTR
Text Label 13400 8450 2    50   ~ 0
IC2_RX
Text Label 13400 8350 2    50   ~ 0
IC2_TX
$Comp
L Connector:TestPoint TP1
U 1 1 5D477326
P 12100 7850
F 0 "TP1" V 12100 8038 50  0000 L CNN
F 1 "TestPoint" V 12145 8038 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 12300 7850 50  0001 C CNN
F 3 "~" H 12300 7850 50  0001 C CNN
	1    12100 7850
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5D4777D1
P 12100 7950
F 0 "TP2" V 12100 8138 50  0000 L CNN
F 1 "TestPoint" V 12145 8138 50  0001 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.0mm" H 12300 7950 50  0001 C CNN
F 3 "~" H 12300 7950 50  0001 C CNN
	1    12100 7950
	0    1    1    0   
$EndComp
NoConn ~ 12100 8150
$Comp
L Device:C_Small C2
U 1 1 5D49A609
P 11500 7000
F 0 "C2" V 11438 6843 50  0000 C CNN
F 1 "C_Small" V 11501 6797 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11500 7000 50  0001 C CNN
F 3 "~" H 11500 7000 50  0001 C CNN
	1    11500 7000
	0    1    1    0   
$EndComp
Wire Wire Line
	11400 7100 11400 7000
Connection ~ 11400 7000
Wire Wire Line
	11400 7000 11400 6850
$Comp
L power:+5V #PWR030
U 1 1 5D4B50EB
P 11700 6850
F 0 "#PWR030" H 11700 6700 50  0001 C CNN
F 1 "+5V" H 11715 7023 50  0000 C CNN
F 2 "" H 11700 6850 50  0001 C CNN
F 3 "" H 11700 6850 50  0001 C CNN
	1    11700 6850
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal_Small Y1
U 1 1 5D4C6906
P 11300 8050
F 0 "Y1" H 11143 8020 50  0000 C CNN
F 1 "Crystal_Small" H 11064 8098 50  0001 C CNN
F 2 "Oscillator:Oscillator_SMD_Abracon_ASE-4Pin_3.2x2.5mm_HandSoldering" H 11300 8050 50  0001 C CNN
F 3 "http://www.farnell.com/datasheets/2645227.pdf" H 11300 8050 50  0001 C CNN
	1    11300 8050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	11100 8150 11100 7950
Text Label 11300 7650 2    50   ~ 0
IC2+D
Text Label 11300 7750 2    50   ~ 0
IC2-D
Text Notes 6800 7900 0    130  ~ 0
[HH:MM:SS]
Wire Wire Line
	13900 6350 14000 6350
Text Label 14600 3800 0    50   ~ 0
HV_MOSI
Text Label 13600 4100 2    50   ~ 0
HV_LE
Text Label 13600 4000 2    50   ~ 0
HV_CLK
Text Label 13400 8550 2    50   ~ 0
APA_SDI
Connection ~ 14000 6350
NoConn ~ 13400 7050
Text Notes 13400 7050 2    50   ~ 0
HV_MISO
$Comp
L power:+5V #PWR037
U 1 1 5D2D72D2
P 14000 3300
F 0 "#PWR037" H 14000 3150 50  0001 C CNN
F 1 "+5V" H 14015 3473 50  0000 C CNN
F 2 "" H 14000 3300 50  0001 C CNN
F 3 "" H 14000 3300 50  0001 C CNN
	1    14000 3300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14100 3300 14200 3300
Wire Wire Line
	14100 5100 14000 5100
$Comp
L power:GND #PWR040
U 1 1 5D2D8A1A
P 14100 5100
F 0 "#PWR040" H 14100 4850 50  0001 C CNN
F 1 "GND" H 14105 4927 50  0001 C CNN
F 2 "" H 14100 5100 50  0001 C CNN
F 3 "" H 14100 5100 50  0001 C CNN
	1    14100 5100
	-1   0    0    -1  
$EndComp
$Comp
L Tube:HV507 U?
U 2 1 5D270FB8
P 7400 7900
AR Path="/5D21978F/5D270FB8" Ref="U?"  Part="2" 
AR Path="/5D270FB8" Ref="U1"  Part="2" 
F 0 "U1" H 7489 6479 50  0000 C CNN
F 1 "HV507" H 7490 6388 50  0000 C CNN
F 2 "Tube:HV507_PQFN080P20X14X0340-80" H 4950 10100 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005845A.pdf" H 4950 10100 50  0001 L CNN
F 4 "Microchip" H 6450 6050 50  0001 L CNN "Fabricant"
F 5 "HV507PG-G" H 6450 5950 50  0001 L CNN "RefFabricant"
F 6 "https://www.microchip.com/wwwproducts/en/HV507" H 6450 5850 50  0001 L CNN "LinkFabricant"
F 7 "Farnell, MICROCHIP" H 6450 5750 50  0001 L CNN "Fournisseur"
F 8 "2920804" H 6450 5650 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2920804" H 6450 5550 50  0001 L CNN "LinkFournisseur"
F 10 "12,52" H 6450 5450 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 6450 5350 50  0001 L CNN "DeviceFournisseur"
	2    7400 7900
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR036
U 1 1 5D2D65F4
P 13600 3700
F 0 "#PWR036" H 13600 3550 50  0001 C CNN
F 1 "+5V" H 13615 3873 50  0000 C CNN
F 2 "" H 13600 3700 50  0001 C CNN
F 3 "" H 13600 3700 50  0001 C CNN
	1    13600 3700
	-1   0    0    -1  
$EndComp
NoConn ~ 14600 3700
Text Label 13400 7150 2    50   ~ 0
HV_CLK
Text Label 13400 6650 2    50   ~ 0
HV_LE
Text Label 13400 6950 2    50   ~ 0
HV_MOSI
Text Notes 13400 6850 2    50   ~ 0
HV_SS
NoConn ~ 13400 6850
$Comp
L power:GND #PWR023
U 1 1 5D303965
P 9950 8100
F 0 "#PWR023" H 9950 7850 50  0001 C CNN
F 1 "GND" H 9955 7927 50  0001 C CNN
F 2 "" H 9950 8100 50  0001 C CNN
F 3 "" H 9950 8100 50  0001 C CNN
	1    9950 8100
	1    0    0    -1  
$EndComp
Text Label 10250 7700 0    50   ~ 0
IC2+D
Text Label 10250 7800 0    50   ~ 0
IC2-D
$Comp
L power:GND #PWR025
U 1 1 5D30438E
P 10250 7900
F 0 "#PWR025" H 10250 7650 50  0001 C CNN
F 1 "GND" H 10255 7727 50  0001 C CNN
F 2 "" H 10250 7900 50  0001 C CNN
F 3 "" H 10250 7900 50  0001 C CNN
	1    10250 7900
	1    0    0    -1  
$EndComp
NoConn ~ 9850 8100
$Comp
L power:+5V #PWR024
U 1 1 5D305AC6
P 10250 7500
F 0 "#PWR024" H 10250 7350 50  0001 C CNN
F 1 "+5V" H 10265 7673 50  0000 C CNN
F 2 "" H 10250 7500 50  0001 C CNN
F 3 "" H 10250 7500 50  0001 C CNN
	1    10250 7500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2700 5350 3350 5350
Wire Wire Line
	2700 5450 3350 5450
Wire Wire Line
	3950 5350 4300 5350
Wire Wire Line
	3950 5450 4300 5450
Wire Wire Line
	4900 5350 5300 5350
Wire Wire Line
	4900 5450 5300 5450
Wire Wire Line
	5900 5350 6400 5350
Wire Wire Line
	5900 5450 6400 5450
Wire Wire Line
	7000 5350 7650 5350
Wire Wire Line
	7000 5450 7650 5450
Wire Wire Line
	8250 5350 8650 5350
Wire Wire Line
	8250 5450 8650 5450
Wire Wire Line
	9250 5350 9650 5350
Wire Wire Line
	9250 5450 9650 5450
Wire Wire Line
	10250 5350 10750 5350
Wire Wire Line
	10250 5450 10750 5450
Wire Wire Line
	11350 5350 12000 5350
Wire Wire Line
	11350 5450 12000 5450
$Comp
L power:GND #PWR04
U 1 1 5D47A92C
P 3000 4650
F 0 "#PWR04" H 3000 4400 50  0001 C CNN
F 1 "GND" H 3005 4477 50  0001 C CNN
F 2 "" H 3000 4650 50  0001 C CNN
F 3 "" H 3000 4650 50  0001 C CNN
	1    3000 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5D47A932
P 3100 4650
F 0 "R2" V 3022 4650 50  0000 C CNN
F 1 "R_Small" V 2995 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3100 4650 50  0001 C CNN
F 3 "~" H 3100 4650 50  0001 C CNN
	1    3100 4650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5D4849F7
P 6050 4650
F 0 "#PWR012" H 6050 4400 50  0001 C CNN
F 1 "GND" H 6055 4477 50  0001 C CNN
F 2 "" H 6050 4650 50  0001 C CNN
F 3 "" H 6050 4650 50  0001 C CNN
	1    6050 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R5
U 1 1 5D4849FD
P 6150 4650
F 0 "R5" V 6072 4650 50  0000 C CNN
F 1 "R_Small" V 6045 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6150 4650 50  0001 C CNN
F 3 "~" H 6150 4650 50  0001 C CNN
	1    6150 4650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5D484A10
P 7300 4650
F 0 "#PWR015" H 7300 4400 50  0001 C CNN
F 1 "GND" H 7305 4477 50  0001 C CNN
F 2 "" H 7300 4650 50  0001 C CNN
F 3 "" H 7300 4650 50  0001 C CNN
	1    7300 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R6
U 1 1 5D484A16
P 7400 4650
F 0 "R6" V 7322 4650 50  0000 C CNN
F 1 "R_Small" V 7295 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 7400 4650 50  0001 C CNN
F 3 "~" H 7400 4650 50  0001 C CNN
	1    7400 4650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5D488397
P 10400 4650
F 0 "#PWR026" H 10400 4400 50  0001 C CNN
F 1 "GND" H 10405 4477 50  0001 C CNN
F 2 "" H 10400 4650 50  0001 C CNN
F 3 "" H 10400 4650 50  0001 C CNN
	1    10400 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R9
U 1 1 5D48839D
P 10500 4650
F 0 "R9" V 10422 4650 50  0000 C CNN
F 1 "R_Small" V 10395 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10500 4650 50  0001 C CNN
F 3 "~" H 10500 4650 50  0001 C CNN
	1    10500 4650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR031
U 1 1 5D4883B0
P 11650 4650
F 0 "#PWR031" H 11650 4400 50  0001 C CNN
F 1 "GND" H 11655 4477 50  0001 C CNN
F 2 "" H 11650 4650 50  0001 C CNN
F 3 "" H 11650 4650 50  0001 C CNN
	1    11650 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R10
U 1 1 5D4883B6
P 11750 4650
F 0 "R10" V 11672 4650 50  0000 C CNN
F 1 "R_Small" V 11645 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 11750 4650 50  0001 C CNN
F 3 "~" H 11750 4650 50  0001 C CNN
	1    11750 4650
	0    1    1    0   
$EndComp
Text Label 4700 4350 2    50   ~ 0
HV_SLT
Text Label 8100 9350 3    50   ~ 0
HV_SLB
Text Label 8000 9350 3    50   ~ 0
HV_SLT
Text Label 8300 9350 3    50   ~ 0
HV_SRB
Text Label 8200 9350 3    50   ~ 0
HV_SRT
Text Label 9050 4450 2    50   ~ 0
HV_SRB
Text Label 9050 4350 2    50   ~ 0
HV_SRT
$Comp
L Tube:IN-16 И1
U 1 1 5D49E109
P 2400 4050
F 0 "И1" H 2345 4884 50  0000 L CNN
F 1 "IN-16" H 2266 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 2400 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 2400 4850 50  0001 C CNN
F 4 "Sovtek" H 2800 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 2800 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 2800 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 2800 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 2800 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 2800 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 2800 3900 50  0001 L CNN "DeviceFournisseur"
	1    2400 4050
	1    0    0    -1  
$EndComp
$Comp
L Tube:CT-IN-18 И3
U 1 1 5D49F948
P 5100 4500
F 0 "И3" H 5050 4862 50  0000 L CNN
F 1 "CT-IN-18" H 4893 4771 50  0000 L CNN
F 2 "Tube:CT-IN-18" H 5100 4150 50  0001 C CNN
F 3 "www.tube-tester.com/pic/eby/cont/data-IN-18 soc-engl.pdf#page11" H 5100 4850 50  0001 C CNN
F 4 "Sovtek" H 5450 4600 50  0001 L CNN "Fournisseur"
F 5 "CT-ИН-18" H 5450 4700 50  0001 L CNN "RefFabricant"
F 6 "CT-NH-18" H 5450 4500 50  0001 L CNN "RefFournisseur"
F 7 "http://www.nocrotec.com/shop/product_info.php/info/p37_2-pcs--CT-IN-18-Colon-Tubes-and-blue-LEDs.html" H 5450 4400 50  0001 L CNN "LinkFournisseur"
F 8 "13,09" H 5450 4300 50  0001 L CNN "CostFournisseur"
F 9 "EUR" H 5450 4200 50  0001 L CNN "DeviceFournisseurDeviceFournisseur"
	1    5100 4500
	1    0    0    -1  
$EndComp
$Comp
L Tube:CT-IN-18 И6
U 1 1 5D4A5573
P 9450 4500
F 0 "И6" H 9400 4862 50  0000 L CNN
F 1 "CT-IN-18" H 9243 4771 50  0000 L CNN
F 2 "Tube:CT-IN-18" H 9450 4150 50  0001 C CNN
F 3 "www.tube-tester.com/pic/eby/cont/data-IN-18 soc-engl.pdf#page11" H 9450 4850 50  0001 C CNN
F 4 "Sovtek" H 9800 4600 50  0001 L CNN "Fournisseur"
F 5 "CT-ИН-18" H 9800 4700 50  0001 L CNN "RefFabricant"
F 6 "CT-NH-18" H 9800 4500 50  0001 L CNN "RefFournisseur"
F 7 "http://www.nocrotec.com/shop/product_info.php/info/p37_2-pcs--CT-IN-18-Colon-Tubes-and-blue-LEDs.html" H 9800 4400 50  0001 L CNN "LinkFournisseur"
F 8 "13,09" H 9800 4300 50  0001 L CNN "CostFournisseur"
F 9 "EUR" H 9800 4200 50  0001 L CNN "DeviceFournisseurDeviceFournisseur"
	1    9450 4500
	1    0    0    -1  
$EndComp
Text Label 3200 3750 2    50   ~ 0
HV_HR3
Text Label 3200 3650 2    50   ~ 0
HV_HR2
Text Label 3200 3550 2    50   ~ 0
HV_HR1
Text Label 3200 3450 2    50   ~ 0
HV_HR0
$Comp
L Tube:IN-16 И2
U 1 1 5D4ABBF9
P 3650 4050
F 0 "И2" H 3595 4884 50  0000 L CNN
F 1 "IN-16" H 3516 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 3650 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 3650 4850 50  0001 C CNN
F 4 "Sovtek" H 4050 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 4050 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 4050 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 4050 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 4050 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 4050 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 4050 3900 50  0001 L CNN "DeviceFournisseur"
	1    3650 4050
	1    0    0    -1  
$EndComp
$Comp
L Tube:IN-16 И5
U 1 1 5D4AF6A1
P 7950 4050
F 0 "И5" H 7895 4884 50  0000 L CNN
F 1 "IN-16" H 7816 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 7950 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 7950 4850 50  0001 C CNN
F 4 "Sovtek" H 8350 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 8350 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 8350 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 8350 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 8350 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 8350 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 8350 3900 50  0001 L CNN "DeviceFournisseur"
	1    7950 4050
	1    0    0    -1  
$EndComp
$Comp
L Tube:IN-16 И4
U 1 1 5D4B103E
P 6700 4050
F 0 "И4" H 6645 4884 50  0000 L CNN
F 1 "IN-16" H 6566 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 6700 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 6700 4850 50  0001 C CNN
F 4 "Sovtek" H 7100 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 7100 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 7100 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 7100 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 7100 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 7100 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 7100 3900 50  0001 L CNN "DeviceFournisseur"
	1    6700 4050
	1    0    0    -1  
$EndComp
$Comp
L Tube:IN-16 И7
U 1 1 5D4B2EF9
P 11050 4050
F 0 "И7" H 10995 4884 50  0000 L CNN
F 1 "IN-16" H 10916 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 11050 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 11050 4850 50  0001 C CNN
F 4 "Sovtek" H 11450 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 11450 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 11450 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 11450 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 11450 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 11450 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 11450 3900 50  0001 L CNN "DeviceFournisseur"
	1    11050 4050
	1    0    0    -1  
$EndComp
$Comp
L Tube:IN-16 И8
U 1 1 5D4B47F0
P 12300 4050
F 0 "И8" H 12245 4884 50  0000 L CNN
F 1 "IN-16" H 12166 4794 50  0000 L CNN
F 2 "Tube:IN-16" H 12300 3250 50  0001 C CNN
F 3 "http://www.tube-tester.com/sites/nixie/dat_arch/IN-16_01.pdf" H 12300 4850 50  0001 C CNN
F 4 "Sovtek" H 12700 4500 50  0001 L CNN "Fabricant"
F 5 "ИН-16" H 12700 4400 50  0001 L CNN "RefFabricant"
F 6 "Nocrotec shop" H 12700 4300 50  0001 L CNN "Fournisseur"
F 7 "IN-16-TUBE" H 12700 4200 50  0001 L CNN "RefFournisseur"
F 8 "http://www.nocrotec.com/shop/product_info.php/info/p30_IN-16-Nixie-Tube---MATCHED-.html" H 12700 4100 50  0001 L CNN "LinkFournisseur"
F 9 "16,18" H 12700 4000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 12700 3900 50  0001 L CNN "DeviceFournisseur"
	1    12300 4050
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J1
U 1 1 5D2F93BC
P 9950 7700
F 0 "J1" H 9955 8167 50  0000 C CNN
F 1 "USB_B_Micro" H 9929 8076 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Wuerth_629105150521_CircularHoles" H 10100 7650 50  0001 C CNN
F 3 "~" H 10100 7650 50  0001 C CNN
	1    9950 7700
	1    0    0    -1  
$EndComp
Wire Wire Line
	13600 3700 13600 3800
$Comp
L Tube:HV507 U?
U 1 1 5D270FAA
P 14100 4150
AR Path="/5D21978F/5D270FAA" Ref="U?"  Part="1" 
AR Path="/5D270FAA" Ref="U1"  Part="1" 
F 0 "U1" H 14289 3240 50  0000 L CNN
F 1 "HV507" H 14250 3148 50  0000 L CNN
F 2 "Tube:HV507_PQFN080P20X14X0340-80" H 11650 6350 50  0001 L CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20005845A.pdf" H 11650 6350 50  0001 L CNN
F 4 "Microchip" H 13150 2300 50  0001 L CNN "Fabricant"
F 5 "HV507PG-G" H 13150 2200 50  0001 L CNN "RefFabricant"
F 6 "https://www.microchip.com/wwwproducts/en/HV507" H 13150 2100 50  0001 L CNN "LinkFabricant"
F 7 "Farnell, MICROCHIP" H 13150 2000 50  0001 L CNN "Fournisseur"
F 8 "2920804" H 13150 1900 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=2920804" H 13150 1800 50  0001 L CNN "LinkFournisseur"
F 10 "12,52" H 13150 1700 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 13150 1600 50  0001 L CNN "DeviceFournisseur"
	1    14100 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5D28C46B
P 1850 4650
F 0 "R1" V 1772 4650 50  0000 C CNN
F 1 "R_Small" V 1745 4650 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1850 4650 50  0001 C CNN
F 3 "~" H 1850 4650 50  0001 C CNN
	1    1850 4650
	0    1    1    0   
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega328-AU U3
U 1 1 5D2398D8
P 14000 7850
F 0 "U3" H 13724 6411 50  0000 C CNN
F 1 "ATmega328-AU" H 13700 6348 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 14000 7850 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 14000 7850 50  0001 L CNN
F 4 "MICROCHIP" H 14000 7850 50  0001 L CNN "Fabricant"
F 5 "ATMEGA328-AU" H 14000 7850 50  0001 L CNN "RefFabricant"
F 6 "https://www.microchip.com/wwwproducts/en/ATmega328" H 14000 7850 50  0001 L CNN "LinkFabricant"
F 7 "Farnell, MICROCHIP" H 14000 7850 50  0001 L CNN "Fournisseur"
F 8 "1972086" H 14000 7850 50  0001 L CNN "RefFournisseur"
F 9 "https://fr.farnell.com/w/search?st=1972086" H 14000 7850 50  0001 L CNN "LinkFournisseur"
F 10 "1,61" H 14000 7850 50  0001 L CNN "CostFournisseur"
F 11 "EUR" H 14000 7850 50  0001 L CNN "DeviceFournisseur"
	1    14000 7850
	-1   0    0    -1  
$EndComp
Text Label 14100 3300 0    50   ~ 0
+167V
$Comp
L power:+5V #PWR03
U 1 1 5DAAFDBD
P 1750 8200
F 0 "#PWR03" H 1750 8050 50  0001 C CNN
F 1 "+5V" H 1765 8389 50  0000 C CNN
F 2 "" H 1750 8200 50  0001 C CNN
F 3 "" H 1750 8200 50  0001 C CNN
	1    1750 8200
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR09
U 1 1 5DAACF89
P 2000 8200
F 0 "#PWR09" H 2000 8050 50  0001 C CNN
F 1 "+5V" H 2015 8389 50  0000 C CNN
F 2 "" H 2000 8200 50  0001 C CNN
F 3 "" H 2000 8200 50  0001 C CNN
	1    2000 8200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5DA9BE15
P 2000 8400
F 0 "#PWR011" H 2000 8150 50  0001 C CNN
F 1 "GND" H 2005 8227 50  0001 C CNN
F 2 "" H 2000 8400 50  0001 C CNN
F 3 "" H 2000 8400 50  0001 C CNN
	1    2000 8400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7
U 1 1 5DA9BE0F
P 2000 8300
F 0 "C7" H 2004 8356 50  0000 L CNN
F 1 "100uF" H 2004 8228 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2000 8300 50  0001 C CNN
F 3 "~" H 2000 8300 50  0001 C CNN
	1    2000 8300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5DA904D3
P 1750 8400
F 0 "#PWR06" H 1750 8150 50  0001 C CNN
F 1 "GND" H 1755 8227 50  0001 C CNN
F 2 "" H 1750 8400 50  0001 C CNN
F 3 "" H 1750 8400 50  0001 C CNN
	1    1750 8400
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5DA904CD
P 1750 8300
F 0 "C6" H 1754 8356 50  0000 L CNN
F 1 "0.1uF" H 1754 8228 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1750 8300 50  0001 C CNN
F 3 "~" H 1750 8300 50  0001 C CNN
	1    1750 8300
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR029
U 1 1 5D6B262C
P 2500 7250
F 0 "#PWR029" H 2500 7100 50  0001 C CNN
F 1 "+5V" H 2515 7439 50  0000 C CNN
F 2 "" H 2500 7250 50  0001 C CNN
F 3 "" H 2500 7250 50  0001 C CNN
	1    2500 7250
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C12
U 1 1 5D79AE36
P 3250 8200
F 0 "C12" V 3207 8220 50  0000 L CNN
F 1 "220pF" V 3306 8222 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3250 8200 50  0001 C CNN
F 3 "~" H 3250 8200 50  0001 C CNN
	1    3250 8200
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C13
U 1 1 5D80403B
P 3500 7800
F 0 "C13" H 3507 7862 50  0000 L CNN
F 1 "1nf" H 3513 7693 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3500 7800 50  0001 C CNN
F 3 "~" H 3500 7800 50  0001 C CNN
	1    3500 7800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5D774DD3
P 3500 7900
F 0 "#PWR043" H 3500 7650 50  0001 C CNN
F 1 "GND" H 3505 7727 50  0001 C CNN
F 2 "" H 3500 7900 50  0001 C CNN
F 3 "" H 3500 7900 50  0001 C CNN
	1    3500 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 8200 4100 8200
Connection ~ 3400 8200
$Comp
L Device:L_Small L1
U 1 1 5D683216
P 2600 7250
F 0 "L1" V 2561 7218 50  0000 L CNN
F 1 "60ohm" V 2655 7080 50  0000 L CNN
F 2 "Diode_SMD:D_0805_2012Metric" H 2600 7250 50  0001 C CNN
F 3 "~" H 2600 7250 50  0001 C CNN
	1    2600 7250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R16
U 1 1 5D799702
P 3250 8050
F 0 "R16" V 3185 7997 50  0000 L CNN
F 1 "NA" V 3246 8015 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3250 8050 50  0001 C CNN
F 3 "~" H 3250 8050 50  0001 C CNN
	1    3250 8050
	0    -1   1    0   
$EndComp
Text Label 5050 7250 0    50   ~ 0
+167V
Connection ~ 4250 7450
Wire Wire Line
	5050 7250 5050 7350
$Comp
L Device:L_Small L3
U 1 1 5D859B3C
P 4950 7250
F 0 "L3" V 4911 7218 50  0000 L CNN
F 1 "60ohm" V 5005 7096 50  0000 L CNN
F 2 "Diode_SMD:D_0805_2012Metric" H 4950 7250 50  0001 C CNN
F 3 "~" H 4950 7250 50  0001 C CNN
	1    4950 7250
	0    1    1    0   
$EndComp
$Comp
L Transistor_FET:FDS2734 Q2
U 1 1 5D60DD76
P 3750 7450
F 0 "Q2" H 3367 7415 50  0000 L CNN
F 1 "FDS2672" H 3366 7494 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3950 7350 50  0001 L CNN
F 3 "http://www.onsemi.com/pub/Collateral/FDS2734-D.pdf" H 3650 7450 50  0001 L CNN
	1    3750 7450
	1    0    0    -1  
$EndComp
Connection ~ 3850 7250
$Comp
L Device:C_Small C10
U 1 1 5D7020DC
P 2650 7900
F 0 "C10" H 2657 7962 50  0000 L CNN
F 1 "430pF" H 2663 7809 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2650 7900 50  0001 C CNN
F 3 "~" H 2650 7900 50  0001 C CNN
	1    2650 7900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR051
U 1 1 5D9ABF76
P 5050 7650
F 0 "#PWR051" H 5050 7400 50  0001 C CNN
F 1 "GND" H 5055 7477 50  0001 C CNN
F 2 "" H 5050 7650 50  0001 C CNN
F 3 "" H 5050 7650 50  0001 C CNN
	1    5050 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C18
U 1 1 5D9A6F40
P 5050 7450
F 0 "C18" H 5054 7506 50  0000 L CNN
F 1 "0.1uF" H 5054 7378 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5050 7450 50  0001 C CNN
F 3 "~" H 5050 7450 50  0001 C CNN
	1    5050 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C17
U 1 1 5D99D2D2
P 4750 7450
F 0 "C17" H 4754 7506 50  0000 L CNN
F 1 "0.1uF" H 4754 7378 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4750 7450 50  0001 C CNN
F 3 "~" H 4750 7450 50  0001 C CNN
	1    4750 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C16
U 1 1 5D997E61
P 4600 7450
F 0 "C16" H 4604 7506 50  0000 L CNN
F 1 "1uF" H 4604 7378 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4600 7450 50  0001 C CNN
F 3 "~" H 4600 7450 50  0001 C CNN
	1    4600 7450
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C15
U 1 1 5D849C10
P 4450 7450
F 0 "C15" H 4454 7506 50  0000 L CNN
F 1 "1uF" H 4454 7378 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4450 7450 50  0001 C CNN
F 3 "~" H 4450 7450 50  0001 C CNN
	1    4450 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 7550 4600 7650
$Comp
L power:GND #PWR048
U 1 1 5D84BF6C
P 4600 7650
F 0 "#PWR048" H 4600 7400 50  0001 C CNN
F 1 "GND" H 4605 7477 50  0001 C CNN
F 2 "" H 4600 7650 50  0001 C CNN
F 3 "" H 4600 7650 50  0001 C CNN
	1    4600 7650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR047
U 1 1 5D849C16
P 4450 7650
F 0 "#PWR047" H 4450 7400 50  0001 C CNN
F 1 "GND" H 4455 7477 50  0001 C CNN
F 2 "" H 4450 7650 50  0001 C CNN
F 3 "" H 4450 7650 50  0001 C CNN
	1    4450 7650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C9
U 1 1 5D65EA44
P 2550 7450
F 0 "C9" V 2593 7477 50  0000 L CNN
F 1 "100uF" V 2500 7480 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2550 7450 50  0001 C CNN
F 3 "~" H 2550 7450 50  0001 C CNN
	1    2550 7450
	0    1    -1   0   
$EndComp
Wire Wire Line
	2700 7450 2650 7450
$Comp
L Device:R_Small R20
U 1 1 5D7EA1D6
P 4250 7350
F 0 "R20" H 4065 7344 50  0000 L CNN
F 1 "634k" V 4246 7297 24  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4250 7350 50  0001 C CNN
F 3 "~" H 4250 7350 50  0001 C CNN
	1    4250 7350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4450 7250 4600 7250
Connection ~ 4450 7250
Wire Wire Line
	4450 7350 4450 7250
Wire Wire Line
	4600 7250 4750 7250
Connection ~ 4600 7250
Wire Wire Line
	4600 7350 4600 7250
Wire Wire Line
	4750 7250 4850 7250
Connection ~ 4750 7250
Wire Wire Line
	4750 7350 4750 7250
$Comp
L power:GND #PWR045
U 1 1 5D931BBD
P 3850 7950
F 0 "#PWR045" H 3850 7700 50  0001 C CNN
F 1 "GND" H 3855 7777 50  0001 C CNN
F 2 "" H 3850 7950 50  0001 C CNN
F 3 "" H 3850 7950 50  0001 C CNN
	1    3850 7950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 8100 3950 8100
$Comp
L Device:R_Small R17
U 1 1 5D63BF19
P 3650 8000
F 0 "R17" V 3579 7955 50  0000 L CNN
F 1 "4.12k" V 3646 7935 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3650 8000 50  0001 C CNN
F 3 "~" H 3650 8000 50  0001 C CNN
	1    3650 8000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 8200 2300 8200
$Comp
L Device:R_Small R13
U 1 1 5D8BD73C
P 2450 8200
F 0 "R13" V 2379 8155 50  0000 L CNN
F 1 "2k" V 2446 8165 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2450 8200 50  0001 C CNN
F 3 "~" H 2450 8200 50  0001 C CNN
	1    2450 8200
	0    -1   1    0   
$EndComp
$Comp
L power:+5V #PWR017
U 1 1 5D8B7753
P 2300 8200
F 0 "#PWR017" H 2300 8050 50  0001 C CNN
F 1 "+5V" H 2315 8389 50  0000 C CNN
F 2 "" H 2300 8200 50  0001 C CNN
F 3 "" H 2300 8200 50  0001 C CNN
	1    2300 8200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4750 7550 4750 7650
$Comp
L power:GND #PWR050
U 1 1 5D84E1ED
P 4750 7650
F 0 "#PWR050" H 4750 7400 50  0001 C CNN
F 1 "GND" H 4755 7477 50  0001 C CNN
F 2 "" H 4750 7650 50  0001 C CNN
F 3 "" H 4750 7650 50  0001 C CNN
	1    4750 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 7550 4450 7650
Wire Wire Line
	5050 7550 5050 7650
$Comp
L power:GND #PWR046
U 1 1 5D83A607
P 4250 7650
F 0 "#PWR046" H 4250 7400 50  0001 C CNN
F 1 "GND" H 4255 7477 50  0001 C CNN
F 2 "" H 4250 7650 50  0001 C CNN
F 3 "" H 4250 7650 50  0001 C CNN
	1    4250 7650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR027
U 1 1 5D8388EF
P 2450 7450
F 0 "#PWR027" H 2450 7200 50  0001 C CNN
F 1 "GND" H 2455 7277 50  0001 C CNN
F 2 "" H 2450 7450 50  0001 C CNN
F 3 "" H 2450 7450 50  0001 C CNN
	1    2450 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 7600 2300 7100
Connection ~ 2300 7600
$Comp
L power:GND #PWR014
U 1 1 5D73E9FA
P 2300 7800
F 0 "#PWR014" H 2300 7550 50  0001 C CNN
F 1 "GND" H 2305 7627 50  0001 C CNN
F 2 "" H 2300 7800 50  0001 C CNN
F 3 "" H 2300 7800 50  0001 C CNN
	1    2300 7800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 7600 2650 7600
$Comp
L Device:C_Small C8
U 1 1 5D6D577F
P 2300 7700
F 0 "C8" H 2366 7650 50  0000 L CNN
F 1 "0.1uF" H 2369 7730 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2300 7700 50  0001 C CNN
F 3 "~" H 2300 7700 50  0001 C CNN
	1    2300 7700
	1    0    0    1   
$EndComp
$Comp
L Device:C_Small C14
U 1 1 5D7BFE81
P 4100 7350
F 0 "C14" H 4129 7205 50  0000 L CNN
F 1 "100pF" V 4012 7380 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4100 7350 50  0001 C CNN
F 3 "~" H 4100 7350 50  0001 C CNN
	1    4100 7350
	1    0    0    1   
$EndComp
Connection ~ 4100 7450
Connection ~ 4100 7250
Wire Wire Line
	4250 7450 4100 7450
Wire Wire Line
	4100 7250 4250 7250
Wire Wire Line
	3900 7250 3850 7250
Wire Wire Line
	4100 8200 4100 7450
Wire Wire Line
	3400 8050 3400 8200
Connection ~ 3400 8050
Wire Wire Line
	3350 8050 3400 8050
Wire Wire Line
	3400 8200 3350 8200
Wire Wire Line
	3400 7750 3400 8050
Wire Wire Line
	3150 8050 3150 8200
$Comp
L Tube:UCC3803 U4
U 1 1 5D60A5FD
P 3050 7650
F 0 "U4" H 3050 8009 50  0000 C CNN
F 1 "UCC3803" H 3050 7945 50  0000 C CNN
F 2 "Tube:SOIC-8-N" H 3700 6800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/ucc3803.pdf" H 3050 8050 50  0001 C CNN
F 4 "TEXAS INSTRUMENTS" H 3400 7500 50  0001 L CNN "Fabricant"
F 5 "UCC3803D" H 3400 7400 50  0001 L CNN "RefFabricant"
F 6 "Farnell" H 3400 7300 50  0001 L CNN "Fournisseur"
F 7 "2323690" H 3400 7200 50  0001 L CNN "RefFournisseur"
F 8 "https://fr.farnell.com/w/search?st=2323690" H 3400 7100 50  0001 L CNN "LinkFournisseur"
F 9 "2,62" H 3400 7000 50  0001 L CNN "CostFournisseur"
F 10 "EUR" H 3400 6900 50  0001 L CNN "DeviceFournisseur"
	1    3050 7650
	1    0    0    -1  
$EndComp
Connection ~ 2700 7250
Wire Wire Line
	3650 7250 2700 7250
Wire Wire Line
	3550 7450 3400 7450
$Comp
L Device:L_Small L2
U 1 1 5D66B9C1
P 3750 7250
F 0 "L2" V 3711 7218 50  0000 L CNN
F 1 "33uH" V 3805 7149 50  0000 L CNN
F 2 "Tube:Choke_SMD_12x12mm_Wurth_WE-PD" H 3750 7250 50  0001 C CNN
F 3 "~" H 3750 7250 50  0001 C CNN
	1    3750 7250
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R18
U 1 1 5D71B312
P 3750 7650
F 0 "R18" V 3679 7605 50  0000 L CNN
F 1 "1k" V 3746 7624 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3750 7650 50  0001 C CNN
F 3 "~" H 3750 7650 50  0001 C CNN
	1    3750 7650
	0    -1   1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5D71C9AB
P 3850 7850
F 0 "R19" V 3779 7805 50  0000 L CNN
F 1 "0.1" V 3846 7815 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 3850 7850 50  0001 C CNN
F 3 "~" H 3850 7850 50  0001 C CNN
	1    3850 7850
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5D7182CD
P 2650 7700
F 0 "R14" H 2679 7756 50  0000 L CNN
F 1 "60.4k" V 2646 7635 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2650 7700 50  0001 C CNN
F 3 "~" H 2650 7700 50  0001 C CNN
	1    2650 7700
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5D7088DF
P 3050 8050
F 0 "#PWR042" H 3050 7800 50  0001 C CNN
F 1 "GND" H 3055 7877 50  0001 C CNN
F 2 "" H 3050 8050 50  0001 C CNN
F 3 "" H 3050 8050 50  0001 C CNN
	1    3050 8050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5D7074C5
P 2650 8000
F 0 "#PWR033" H 2650 7750 50  0001 C CNN
F 1 "GND" H 2655 7827 50  0001 C CNN
F 2 "" H 2650 8000 50  0001 C CNN
F 3 "" H 2650 8000 50  0001 C CNN
	1    2650 8000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 7250 2700 7450
Text Notes 1050 9250 0    50   ~ 0
https://surfncircuits.com/2018/02/03/optimizing-the-5v-to-170v-nixie-tube-power-supply-design-part-2
Text Notes 1950 9150 0    50   ~ 0
Optimizing the 5v to 170v Nixie Tube Power Supply Design
$Comp
L power:GND #PWR059
U 1 1 5D6F2F14
P 11700 8350
F 0 "#PWR059" H 11700 8100 50  0001 C CNN
F 1 "GND" H 11705 8177 50  0001 C CNN
F 2 "" H 11700 8350 50  0001 C CNN
F 3 "" H 11700 8350 50  0001 C CNN
	1    11700 8350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR058
U 1 1 5D6F7479
P 11400 7100
F 0 "#PWR058" H 11400 6850 50  0001 C CNN
F 1 "GND" H 11405 6927 50  0001 C CNN
F 2 "" H 11400 7100 50  0001 C CNN
F 3 "" H 11400 7100 50  0001 C CNN
	1    11400 7100
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C1
U 1 1 5D49E7A5
P 11500 6850
F 0 "C1" V 11456 6758 50  0000 C CNN
F 1 "CP1_Small" V 11520 6661 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11500 6850 50  0001 C CNN
F 3 "~" H 11500 6850 50  0001 C CNN
	1    11500 6850
	0    1    1    0   
$EndComp
Wire Wire Line
	11700 7000 11700 6850
$Comp
L Device:C_Small C3
U 1 1 5D7307ED
P 11500 7100
F 0 "C3" V 11438 6943 50  0000 C CNN
F 1 "C_Small" V 11501 6897 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11500 7100 50  0001 C CNN
F 3 "~" H 11500 7100 50  0001 C CNN
	1    11500 7100
	0    1    1    0   
$EndComp
Connection ~ 11400 7100
Wire Wire Line
	11600 7000 11700 7000
Wire Wire Line
	11600 6850 11700 6850
Connection ~ 11700 6850
Wire Wire Line
	11700 7000 11700 7150
Connection ~ 11700 7000
Wire Wire Line
	11600 7100 11600 7150
$Comp
L Device:C_Small C5
U 1 1 5D74DC0A
P 11200 8150
F 0 "C5" V 11138 7993 50  0000 C CNN
F 1 "C_Small" V 11201 7947 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11200 8150 50  0001 C CNN
F 3 "~" H 11200 8150 50  0001 C CNN
	1    11200 8150
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5D752BE4
P 11200 7950
F 0 "C4" V 11138 7793 50  0000 C CNN
F 1 "C_Small" V 11201 7747 50  0001 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 11200 7950 50  0001 C CNN
F 3 "~" H 11200 7950 50  0001 C CNN
	1    11200 7950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5D75A713
P 2400 5750
F 0 "#PWR022" H 2400 5500 50  0001 C CNN
F 1 "GND" H 2405 5577 50  0001 C CNN
F 2 "" H 2400 5750 50  0001 C CNN
F 3 "" H 2400 5750 50  0001 C CNN
	1    2400 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR044
U 1 1 5D75C213
P 3650 5750
F 0 "#PWR044" H 3650 5500 50  0001 C CNN
F 1 "GND" H 3655 5577 50  0001 C CNN
F 2 "" H 3650 5750 50  0001 C CNN
F 3 "" H 3650 5750 50  0001 C CNN
	1    3650 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR049
U 1 1 5D75DB7A
P 4600 5750
F 0 "#PWR049" H 4600 5500 50  0001 C CNN
F 1 "GND" H 4605 5577 50  0001 C CNN
F 2 "" H 4600 5750 50  0001 C CNN
F 3 "" H 4600 5750 50  0001 C CNN
	1    4600 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5D75F515
P 5600 5750
F 0 "#PWR052" H 5600 5500 50  0001 C CNN
F 1 "GND" H 5605 5577 50  0001 C CNN
F 2 "" H 5600 5750 50  0001 C CNN
F 3 "" H 5600 5750 50  0001 C CNN
	1    5600 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR053
U 1 1 5D76109D
P 6700 5750
F 0 "#PWR053" H 6700 5500 50  0001 C CNN
F 1 "GND" H 6705 5577 50  0001 C CNN
F 2 "" H 6700 5750 50  0001 C CNN
F 3 "" H 6700 5750 50  0001 C CNN
	1    6700 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR054
U 1 1 5D762AE2
P 7950 5750
F 0 "#PWR054" H 7950 5500 50  0001 C CNN
F 1 "GND" H 7955 5577 50  0001 C CNN
F 2 "" H 7950 5750 50  0001 C CNN
F 3 "" H 7950 5750 50  0001 C CNN
	1    7950 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5D764417
P 8950 5750
F 0 "#PWR055" H 8950 5500 50  0001 C CNN
F 1 "GND" H 8955 5577 50  0001 C CNN
F 2 "" H 8950 5750 50  0001 C CNN
F 3 "" H 8950 5750 50  0001 C CNN
	1    8950 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR056
U 1 1 5D765E5C
P 9950 5750
F 0 "#PWR056" H 9950 5500 50  0001 C CNN
F 1 "GND" H 9955 5577 50  0001 C CNN
F 2 "" H 9950 5750 50  0001 C CNN
F 3 "" H 9950 5750 50  0001 C CNN
	1    9950 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 5D767A4A
P 11050 5750
F 0 "#PWR057" H 11050 5500 50  0001 C CNN
F 1 "GND" H 11055 5577 50  0001 C CNN
F 2 "" H 11050 5750 50  0001 C CNN
F 3 "" H 11050 5750 50  0001 C CNN
	1    11050 5750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5D769407
P 12300 5750
F 0 "#PWR060" H 12300 5500 50  0001 C CNN
F 1 "GND" H 12305 5577 50  0001 C CNN
F 2 "" H 12300 5750 50  0001 C CNN
F 3 "" H 12300 5750 50  0001 C CNN
	1    12300 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R8
U 1 1 5D311840
P 8950 4650
F 0 "R8" V 8930 4482 50  0000 C CNN
F 1 "R_Small" V 9042 4689 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8950 4650 50  0001 C CNN
F 3 "~" H 8950 4650 50  0001 C CNN
	1    8950 4650
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7
U 1 1 5D311831
P 8950 4550
F 0 "R7" V 8950 4380 50  0000 C CNN
F 1 "R_Small" V 8845 4550 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8950 4550 50  0001 C CNN
F 3 "~" H 8950 4550 50  0001 C CNN
	1    8950 4550
	0    1    1    0   
$EndComp
Connection ~ 11300 7950
Connection ~ 11300 8150
Connection ~ 13600 3700
Connection ~ 13600 3800
Wire Wire Line
	13600 3800 13600 3900
Connection ~ 14100 5100
Text Notes 10200 8700 0    130  ~ 0
USB⇒UART
Text Notes 3450 8700 0    130  ~ 0
5V⇒170V
$Comp
L power:GND #PWR032
U 1 1 5D8B52A5
P 2600 8600
F 0 "#PWR032" H 2600 8350 50  0001 C CNN
F 1 "GND" H 2605 8427 50  0001 C CNN
F 2 "" H 2600 8600 50  0001 C CNN
F 3 "" H 2600 8600 50  0001 C CNN
	1    2600 8600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5D8D3A73
P 2750 8400
F 0 "#PWR035" H 2750 8150 50  0001 C CNN
F 1 "GND" H 2755 8227 50  0001 C CNN
F 2 "" H 2750 8400 50  0001 C CNN
F 3 "" H 2750 8400 50  0001 C CNN
	1    2750 8400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR041
U 1 1 5D8D83A3
P 2900 8400
F 0 "#PWR041" H 2900 8150 50  0001 C CNN
F 1 "GND" H 2905 8227 50  0001 C CNN
F 2 "" H 2900 8400 50  0001 C CNN
F 3 "" H 2900 8400 50  0001 C CNN
	1    2900 8400
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_EBC Q1
U 1 1 5D61DCF2
P 2500 8400
F 0 "Q1" H 2495 8242 50  0000 L CNN
F 1 "MMBTA42,215" H 2691 8355 50  0001 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2700 8500 50  0001 C CNN
F 3 "~" H 2500 8400 50  0001 C CNN
	1    2500 8400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R15
U 1 1 5D9BD197
P 2750 8300
F 0 "R15" H 2745 8412 50  0000 L CNN
F 1 "100k" V 2746 8236 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2750 8300 50  0001 C CNN
F 3 "~" H 2750 8300 50  0001 C CNN
	1    2750 8300
	1    0    0    1   
$EndComp
Connection ~ 2900 8200
$Comp
L power:GND #PWR020
U 1 1 5D8B32AB
P 2300 8800
F 0 "#PWR020" H 2300 8550 50  0001 C CNN
F 1 "GND" H 2305 8627 50  0001 C CNN
F 2 "" H 2300 8800 50  0001 C CNN
F 3 "" H 2300 8800 50  0001 C CNN
	1    2300 8800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 7250 4450 7250
Connection ~ 4250 7250
$Comp
L Device:R_Small R21
U 1 1 5D96D71D
P 4250 7550
F 0 "R21" H 4066 7544 50  0000 L CNN
F 1 "7.68k" V 4246 7492 24  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4250 7550 50  0001 C CNN
F 3 "~" H 4250 7550 50  0001 C CNN
	1    4250 7550
	-1   0    0    -1  
$EndComp
$Comp
L Device:D_Small D12
U 1 1 5D7CC8C1
P 4000 7250
F 0 "D12" H 3961 7363 50  0000 C CNN
F 1 "ES1D" H 4000 7364 50  0001 C CNN
F 2 "Diode_SMD:D_SMA" V 4000 7250 50  0001 C CNN
F 3 "~" V 4000 7250 50  0001 C CNN
	1    4000 7250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3650 7650 3650 7900
Connection ~ 3650 7650
Wire Wire Line
	3950 7100 3950 8100
Wire Wire Line
	3850 7650 3850 7750
Connection ~ 3850 7650
Wire Wire Line
	3400 7650 3500 7650
Wire Wire Line
	3500 7700 3500 7650
Connection ~ 3500 7650
Wire Wire Line
	3500 7650 3650 7650
Connection ~ 2700 7450
Connection ~ 3150 8050
Wire Wire Line
	2700 7800 2650 7800
Connection ~ 2650 7800
Wire Wire Line
	2650 7600 2700 7600
Connection ~ 2650 7600
Wire Wire Line
	2300 7100 3950 7100
Wire Wire Line
	3150 8200 3100 8200
Connection ~ 3150 8200
Connection ~ 2750 8200
Wire Wire Line
	2750 8200 2900 8200
$Comp
L Device:C_Small C11
U 1 1 5D9C8100
P 2900 8300
F 0 "C11" H 2965 8299 50  0000 L CNN
F 1 "47uF" H 2903 8189 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2900 8300 50  0001 C CNN
F 3 "~" H 2900 8300 50  0001 C CNN
	1    2900 8300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R11
U 1 1 5D8AE03D
P 2300 8300
F 0 "R11" V 2229 8216 50  0000 L CNN
F 1 "100k" V 2296 8247 31  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 2300 8300 50  0001 C CNN
F 3 "~" H 2300 8300 50  0001 C CNN
	1    2300 8300
	-1   0    0    -1  
$EndComp
Connection ~ 2300 8200
$Comp
L Device:D_Schottky_Small D11
U 1 1 5D8A2A48
P 3000 8200
F 0 "D11" H 2886 8268 50  0000 C CNN
F 1 "D_Schottky_Small" V 3045 8268 50  0001 L CNN
F 2 "Diode_SMD:D_SOD-323" V 3000 8200 50  0001 C CNN
F 3 "~" V 3000 8200 50  0001 C CNN
	1    3000 8200
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT?
U 1 1 5D676C5E
P 10250 6650
F 0 "BT?" H 10368 6746 50  0000 L CNN
F 1 "Battery_Cell" H 10368 6655 50  0000 L CNN
F 2 "" V 10250 6710 50  0001 C CNN
F 3 "~" V 10250 6710 50  0001 C CNN
	1    10250 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1 C?
U 1 1 5D68990C
P 9850 6600
F 0 "C?" H 9965 6646 50  0000 L CNN
F 1 "10uF" H 9965 6555 50  0000 L CNN
F 2 "" H 9850 6600 50  0001 C CNN
F 3 "~" H 9850 6600 50  0001 C CNN
	1    9850 6600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D6907EF
P 9850 6750
F 0 "#PWR?" H 9850 6500 50  0001 C CNN
F 1 "GND" H 9855 6577 50  0001 C CNN
F 2 "" H 9850 6750 50  0001 C CNN
F 3 "" H 9850 6750 50  0001 C CNN
	1    9850 6750
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D69262F
P 10250 6750
F 0 "#PWR?" H 10250 6500 50  0001 C CNN
F 1 "GND" H 10255 6577 50  0001 C CNN
F 2 "" H 10250 6750 50  0001 C CNN
F 3 "" H 10250 6750 50  0001 C CNN
	1    10250 6750
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D69480B
P 9850 6450
F 0 "#PWR?" H 9850 6300 50  0001 C CNN
F 1 "+5V" H 9865 6623 50  0000 C CNN
F 2 "" H 9850 6450 50  0001 C CNN
F 3 "" H 9850 6450 50  0001 C CNN
	1    9850 6450
	-1   0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5D696651
P 10250 6450
F 0 "#PWR?" H 10250 6300 50  0001 C CNN
F 1 "+5V" H 10265 6623 50  0000 C CNN
F 2 "" H 10250 6450 50  0001 C CNN
F 3 "" H 10250 6450 50  0001 C CNN
	1    10250 6450
	-1   0    0    -1  
$EndComp
Text Label 13400 8650 2    50   ~ 0
APA_CLK
Text Label 13400 8750 2    50   ~ 0
IC2_XLK
Wire Wire Line
	13000 8950 13000 9050
Connection ~ 13000 8950
Connection ~ 13000 9050
Wire Wire Line
	13000 8850 13000 8950
$Comp
L power:GND #PWR?
U 1 1 5D6E435B
P 13000 9050
F 0 "#PWR?" H 13000 8800 50  0001 C CNN
F 1 "GND" H 13005 8877 50  0001 C CNN
F 2 "" H 13000 9050 50  0001 C CNN
F 3 "" H 13000 9050 50  0001 C CNN
	1    13000 9050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5D6E2289
P 13200 9050
F 0 "SW?" H 12913 9099 50  0000 C CNN
F 1 "SW_Push" H 13200 9244 50  0001 C CNN
F 2 "" H 13200 9250 50  0001 C CNN
F 3 "~" H 13200 9250 50  0001 C CNN
	1    13200 9050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5D6E0006
P 13200 8850
F 0 "SW?" H 12913 8899 50  0000 C CNN
F 1 "SW_Push" H 13200 9044 50  0001 C CNN
F 2 "" H 13200 9050 50  0001 C CNN
F 3 "~" H 13200 9050 50  0001 C CNN
	1    13200 8850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5D6CEC3C
P 13200 8950
F 0 "SW?" H 12913 8999 50  0000 C CNN
F 1 "SW_Push" H 13200 9144 50  0001 C CNN
F 2 "" H 13200 9150 50  0001 C CNN
F 3 "~" H 13200 9150 50  0001 C CNN
	1    13200 8950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW?
U 1 1 5D713B76
P 2300 8600
F 0 "SW?" V 2421 8747 50  0000 C CNN
F 1 "SW_SPST" H 2300 8744 50  0001 C CNN
F 2 "" H 2300 8600 50  0001 C CNN
F 3 "~" H 2300 8600 50  0001 C CNN
	1    2300 8600
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 8200 2600 8200
Connection ~ 2600 8200
Wire Wire Line
	2600 8200 2750 8200
Connection ~ 2300 8400
$EndSCHEMATC
